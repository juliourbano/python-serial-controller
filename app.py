from local_serial.reader import Reader
from local_serial.writer import Writer
from queue import Queue
from control.constant import ConstantProcessor
from control.integrator import Integrator
from control.derivative import Derivative
from control.pid import PID
import serial
import time

class app:
    def __init__(self) -> None:
        self.parsersPipe = []
        self.serial = None
        self.port = None
        self.sampleTime = 10
        self.reader = None
        self.writer = None
        self.queue = Queue()
        self.dbg = None
        # params of the controller
        self.controller = None
        self.kp = 1.0
        self.ki = 1.0
        self.kd = 0.0

    def addParser(self, parser, object):
        self.parsersPipe.append({ "parser": parser, "object": object })

    def parse(self):
        if len(self.parsersPipe) == 0:
            raise Exception("Must add at least command parser")
        
        for parser in self.parsersPipe:
            parser["parser"].parse(parser["object"])

    def init(self):
        print("Parsing commands")
        self.parse()
        '''
        Currently with default values
        Must add params to create a customized one
        '''

        print("Inferring the controller")
        controllerObject = None
        if self.controller == 'constant':
            controllerObject = ConstantProcessor(self.kp)
        elif self.controller == 'integrator':
            controllerObject = Integrator(self.ki, (self.sampleTime / 1000))
        elif self.controller == 'derivative':
            controllerObject = Derivative(self.kd, (self.sampleTime / 1000))
        elif self.controller == 'pid':
            controllerObject = PID(self.kp, self.ki, self.kd, (self.sampleTime / 1000))
        else:
            raise Exception("Unable to infer the controller")

        print("Controller: ", controllerObject)

        print("Creating the serial object")
        self.serial = serial.Serial(self.port)
        self.reader = Reader(self.serial, self.queue, self.dbg)
        self.writer = Writer(self.serial, self.queue, controllerObject, (self.sampleTime / 1000), self.dbg)

        print("Opening serial connection")
        '''
        If the port is already open, application will force it
        to close
        '''
        if self.serial.is_open:
            self.serial.close()

        self.serial.open()
        self.serial.reset_input_buffer()
        self.serial.reset_output_buffer()

        self.reader.start()
        self.writer.start()

        self.debug()

        while 1:
            time.sleep(1)

    def debug(self):
        print("Port selected: ", self.port)
        print("Sample time selected: ", self.sampleTime)
        print("DBG option: ", self.dbg)

    def run(self):
        self.init()
