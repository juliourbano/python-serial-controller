from control.processor import Processor

class ConstantProcessor(Processor):
    def __init__(self, k: float) -> None:
        self.k = k
    
    def process(self, val: float) -> float:
        return val * self.k
    
    def __str__(self) -> str:
        return "Constant(Kp="+str(self.k)+")"