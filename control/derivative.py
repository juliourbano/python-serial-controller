from control.processor import Processor

class Derivative(Processor):
    def __init__(self, k: float, sampleTime: float) -> None:
        self.k = k
        self.N = 100.0
        self.sampleTime = sampleTime

        self.currentValue = 0.0
        self.pastValue = 0.0
        self.pastErrorValue = 0.0
    
    def process(self, val: float) -> float:
        Kd_tn_kd = self.k / (self.sampleTime*self.N + self.k)
        N_kd_tn_kd = self.N * Kd_tn_kd
        
        error = val - self.pastErrorValue

        self.currentValue = N_kd_tn_kd*error + Kd_tn_kd*self.pastValue
        
        self.pastErrorValue = val
        self.pastValue = self.currentValue

        return self.currentValue
    
    def __str__(self) -> str:
        return "Derivative(Kd="+str(self.k)+", N: "+str(self.N)+", sample time: "+str(self.sampleTime)+")"
        