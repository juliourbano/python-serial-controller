from control.processor import Processor

class Integrator(Processor):
    def __init__(self, k: float, sampleTime: float) -> None:
        self.k = k
        self.currentValue = 0.0
        self.sampleTime = sampleTime
    
    def process(self, val: float) -> float:
        self.currentValue = self.currentValue + (self.k * val * self.sampleTime)
        return self.currentValue
    
    def __str__(self) -> str:
        return "Integrator(Ki="+str(self.k)+", sample time: "+str(self.sampleTime)+")"
        