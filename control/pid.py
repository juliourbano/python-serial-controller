from control.processor import Processor
from control.constant import ConstantProcessor
from control.integrator import Integrator
from control.derivative import Derivative

class PID(Processor):
    def __init__(self, kp: float, ki: float, kd: float, sampleTime: float) -> None:
        self.constant = ConstantProcessor(kp)
        self.integrator = Integrator(ki, sampleTime)
        self.derivative = Derivative(kd, sampleTime)
    
    def process(self, val: float) -> float:
        return self.constant.process(val) + self.integrator.process(val) + self.derivative.process(val)
    
    def __str__(self) -> str:
        return str(self.constant)+", "+str(self.integrator)+", "+str(self.derivative)
        