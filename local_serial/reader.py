import threading
import time
from serial import Serial
from queue import Queue
import struct


class Reader(threading.Thread):

    def __init__(self,
                 serial: Serial,
                 queue: Queue,
                 deb: bool = False) -> None:
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self.serial = serial
        self.queue = queue
        self.deb = deb

    def run(self) -> None:
        print("Starting reading thread")
        lock = threading.Lock()
        while 1:
            lock.acquire()
            if self.serial.in_waiting > 0:
                val = self.serial.read_until(terminator=b'\n')
                if self.deb:
                    print("readed raw: ", val, "bytes count: ", len(val))

                if len(val) == 9:
                    float_val, = struct.unpack('d', val[0:8])

                    if self.deb:
                        print("readed value: ", float_val)

                    self.queue.put(float_val)
            lock.release()

            time.sleep(0.001)