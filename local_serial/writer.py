import threading
import time
from queue import Queue
from control.processor import Processor
from serial import Serial
import struct

class Writer(threading.Thread):
    def __init__(self, serial: Serial, queue: Queue, processor: Processor, sample: float = 0.01, deb: bool= False)-> None:
        threading.Thread.__init__(self)
        self.setDaemon(True)
        self.serial = serial
        self.queue = queue
        self.processor = processor
        self.last_val = 0.0
        self.sample = sample
        self.__time = 0.0
        self.__int_step_size = 0.001
        self.deb = deb
    
    def run(self) -> None:
        print("Starting writing thread with sample: ", self.sample)
        lock = threading.Lock()
        while 1:
            if not self.queue.empty():
                last_val = self.queue.get()
                
                if self.deb:
                    print("last val: ", last_val)

                next_val = self.processor.process(last_val)
                # update the memory last value
                self.last_val = next_val

            lock.acquire()
            self.serial.write(struct.pack('d', self.last_val) + b'\n')
            #self.serial.flush()
            lock.release()

            self.__time += self.__int_step_size
            time.sleep(self.sample)