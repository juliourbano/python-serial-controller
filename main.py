from app import app
from parsing.commands import CommandsParser

if __name__ == '__main__':
    commandsParser = CommandsParser()
    app = app()

    app.addParser(commandsParser, app)

    try:
        app.run()
    except Exception as e:
        print("Error: ", e)