import argparse

class CommandsParser:
    def __init__(self) -> None:
        self.parser = argparse.ArgumentParser(description="Commands args parser")
        self.parser.add_argument("-p", "--port")
        self.parser.add_argument("-s", "--sample", type=int)
        self.parser.add_argument("-d", "--debug")
        self.parser.add_argument("-c", "--controller", type=str, choices=['constant', 'integrator', 'derivative', 'pid'])
        self.parser.add_argument("-kp", "--kp", type=float)
        self.parser.add_argument("-kd", "--kd", type=float)
        self.parser.add_argument("-ki", "--ki", type=float)

    def parse(self, appObject):
        args = self.parser.parse_args()
        print("args: ", args)

        if args.port:
            print("[Port] param found with value: ", args.port)
            appObject.port = args.port
        else:
            raise Exception("[Port] param is required")
        if args.sample and args.sample >= 10:
            print("[Sample] param found with value: ", args.sample)
            appObject.sampleTime = args.sample
        else:
            print("[Sample] param set to default value: 10")
            appObject.sampleTime = 10
        if args.debug is not None:
            print("[Debug] param present: ", args.debug == 'true')
            appObject.dbg = args.debug == 'true'
        # check controller params
        if args.controller:
            print("[Controller] param set to ", args.controller)
            appObject.controller = args.controller
        else:
            raise Exception("[Controller] params is required. Values: constant, integrator")
        if args.kp is not None:
            print("[Kp] param set to: ", args.kp)
            appObject.kp = args.kp
        if args.ki is not None:
            print("[Ki] param set to: ", args.ki)
            appObject.ki = args.ki
        if args.kd is not None:
            print("[Kd] param set to: ", args.kd)
            appObject.kd = args.kd
